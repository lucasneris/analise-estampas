-------------------------------------------------------------------------------------------------------------------
---------------------------------------------VERAO 20--------------------------------------------------------------

SELECT
CONCAT(RTRIM(LTRIM(COMPRAS.PRODUTO)),RTRIM(LTRIM(COMPRAS.COR_PRODUTO))) as REF,

CAST(COMPRAS.DATA_ENTREGA as Date) as DATA,
SUM(COMPRAS.QTDE) as QTDE,
PROD_PRECO_VENDA.PRECO1 AS PRECO_UNIT,
'VER20' AS COLECAO,
COMPRAS.TIPO_PROGRAMACAO as TIPO_PROGRAMACAO,
'COMPRA' as TIPO_TABELA

FROM 

WANM_PROGRAMACOES_RELLINX COMPRAS
 
JOIN PRODUTOS PROD 
ON COMPRAS.PRODUTO = PROD.PRODUTO

JOIN PRODUTOS_PRECOS PROD_PRECO_VENDA
 ON COMPRAS.PRODUTO = PROD_PRECO_VENDA.PRODUTO AND PROD_PRECO_VENDA.CODIGO_TAB_PRECO = 'VO'

WHERE 
TIPO_PROGRAMACAO IN ('VAREJO', 'REPIQUE VAREJO') AND
programacao like ('%v20%') AND
PROD.REDE_LOJAS='1'


GROUP BY CONCAT(RTRIM(LTRIM(COMPRAS.PRODUTO)),RTRIM(LTRIM(COMPRAS.COR_PRODUTO))), 
         PROD_PRECO_VENDA.PRECO1,
         COMPRAS.TIPO_PROGRAMACAO,
        CAST(COMPRAS.DATA_ENTREGA as Date)

-------------------------------------------------------------------------------------------------------------------
UNION
---------------------------------------------VERAO 19--------------------------------------------------------------

SELECT  CONCAT(RTRIM(LTRIM(PRODUCAO_ORDEM_COR.PRODUTO)),RTRIM(LTRIM(PRODUCAO_ORDEM_COR.COR_PRODUTO))) as REF,

		PRODUCAO_PROG_PROD.ENTREGA_INICIAL as DATA,
		SUM(PRODUCAO_ORDEM_COR.QTDE_O) as QTDE,
		PROD_PRECO_VENDA.PRECO1 AS PRECO_UNIT,
		'VER19' as COLECAO,
		PRODUCAO_ORDEM.TIPO_ORDEM_PRODUCAO as TIPO_PROGRAMACAO,
		'COMPRA' as TIPO_TABELA
		
		
         FROM PRODUCAO_ORDEM_COR
         JOIN PRODUCAO_ORDEM
             ON PRODUCAO_ORDEM_COR.ORDEM_PRODUCAO = PRODUCAO_ORDEM.ORDEM_PRODUCAO AND PRODUCAO_ORDEM_COR.PRODUTO = PRODUCAO_ORDEM.PRODUTO 
         LEFT JOIN PRODUTOS  
             ON PRODUCAO_ORDEM_COR.PRODUTO = PRODUTOS.PRODUTO 
		 LEFT JOIN PRODUTOS_PRECOS PROD_PRECO_VENDA
			 ON PRODUCAO_ORDEM_COR.PRODUTO = PROD_PRECO_VENDA.PRODUTO AND PROD_PRECO_VENDA.CODIGO_TAB_PRECO = 'VO'
		LEFT JOIN PRODUCAO_PROG_PROD
			 ON PRODUCAO_ORDEM_COR.PRODUTO = PRODUCAO_PROG_PROD.PRODUTO AND PRODUCAO_ORDEM_COR.COR_PRODUTO = PRODUCAO_PROG_PROD.COR_PRODUTO AND PRODUCAO_ORDEM.PROGRAMACAO = PRODUCAO_PROG_PROD.PROGRAMACAO

        WHERE   PRODUCAO_ORDEM.PROGRAMACAO IN    (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                      WHERE ((PROPRIEDADE = '00060' 
                                             AND VALOR_PROPRIEDADE = 'VERAO 19') OR PROGRAMACAO LIKE '%V19%' OR PROGRAMACAO LIKE '%VER19%') 
                                             AND PROGRAMACAO IN (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                                                 WHERE PROPRIEDADE = '00038' 
                                                                 AND VALOR_PROPRIEDADE IN ('VAREJO','REPIQUE VAREJO'))) 
                 AND PRODUTOS.REDE_LOJAS = 1 
                 AND PRODUCAO_ORDEM.TIPO_PROCESSO = 0 
				 AND PRODUCAO_ORDEM_COR.QTDE_O <> 0

         GROUP BY PRODUCAO_ORDEM_COR.PRODUTO,
				  PRODUCAO_ORDEM_COR.COR_PRODUTO, 
				  PRODUCAO_PROG_PROD.ENTREGA_INICIAL,
				  PROD_PRECO_VENDA.PRECO1,
				  PRODUCAO_ORDEM.TIPO_ORDEM_PRODUCAO

union
     
         SELECT  CONCAT(RTRIM(LTRIM(COMPRAS_PRODUTO.PRODUTO)),RTRIM(LTRIM(COMPRAS_PRODUTO.COR_PRODUTO))) as REF,
         
				 COMPRAS_PRODUTO.ENTREGA as DATA,
				 SUM(COMPRAS_PRODUTO.QTDE_ORIGINAL) - ISNULL(SUM(COMPRAS_PROD_CANCELADA.QTDE_CANCELADA), 0) as QTDE,
				 PROD_PRECO_VENDA.PRECO1 AS PRECO_UNIT,
				 'VER19'  as COLECAO,
				 COMPRAS.TIPO_COMPRA as TIPO_PROGRAMACAO,
				 'COMPRA' as TIPO_TABELA
				 

         FROM COMPRAS_PRODUTO

         JOIN COMPRAS
             ON COMPRAS_PRODUTO.PEDIDO = COMPRAS.PEDIDO

         LEFT JOIN PRODUTOS
             ON COMPRAS_PRODUTO.PRODUTO = PRODUTOS.PRODUTO

         FULL JOIN COMPRAS_PROD_CANCELADA 
             ON COMPRAS_PRODUTO.PRODUTO = COMPRAS_PROD_CANCELADA.PRODUTO AND COMPRAS_PRODUTO.COR_PRODUTO = COMPRAS_PROD_CANCELADA.COR_PRODUTO AND COMPRAS_PRODUTO.PEDIDO = COMPRAS_PROD_CANCELADA.PEDIDO

		LEFT JOIN PRODUTOS_PRECOS PROD_PRECO_VENDA
			 ON COMPRAS_PRODUTO.PRODUTO = PROD_PRECO_VENDA.PRODUTO AND PROD_PRECO_VENDA.CODIGO_TAB_PRECO = 'VO'

         WHERE   COMPRAS.PROGRAMACAO IN    (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                      WHERE ((PROPRIEDADE = '00060' 
                                             AND VALOR_PROPRIEDADE = 'VERAO 19') OR PROGRAMACAO LIKE '%V19%' OR PROGRAMACAO LIKE '%VER19%') 
                                             AND PROGRAMACAO IN (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                                                 WHERE PROPRIEDADE = '00038' 
                                                                 AND VALOR_PROPRIEDADE IN ('VAREJO','REPIQUE VAREJO'))) 
                 AND PRODUTOS.REDE_LOJAS = 1 
				 AND COMPRAS_PRODUTO.QTDE_ORIGINAL <> 0
				 AND COMPRAS_PRODUTO.QTDE_CANCELADA <> COMPRAS_PRODUTO.QTDE_ORIGINAL

         GROUP BY COMPRAS_PRODUTO.PRODUTO,
				  COMPRAS_PRODUTO.COR_PRODUTO,
				  COMPRAS_PRODUTO.ENTREGA,
				  PROD_PRECO_VENDA.PRECO1,
				  --,
				  COMPRAS.TIPO_COMPRA

-------------------------------------------------------------------------------------------------------------------
UNION
---------------------------------------------VERAO 18--------------------------------------------------------------

SELECT  CONCAT(RTRIM(LTRIM(PRODUCAO_ORDEM_COR.PRODUTO)),RTRIM(LTRIM(PRODUCAO_ORDEM_COR.COR_PRODUTO))) as REF,

		PRODUCAO_PROG_PROD.ENTREGA_INICIAL as DATA,
		SUM(PRODUCAO_ORDEM_COR.QTDE_O) as QTDE,
		PROD_PRECO_VENDA.PRECO1 AS PRECO_UNIT,
		'VER18' as COLECAO,
		PRODUCAO_ORDEM.TIPO_ORDEM_PRODUCAO as TIPO_PROGRAMACAO,
		'COMPRA' as TIPO_TABELA
		
		
         FROM PRODUCAO_ORDEM_COR
         JOIN PRODUCAO_ORDEM
             ON PRODUCAO_ORDEM_COR.ORDEM_PRODUCAO = PRODUCAO_ORDEM.ORDEM_PRODUCAO AND PRODUCAO_ORDEM_COR.PRODUTO = PRODUCAO_ORDEM.PRODUTO 
         LEFT JOIN PRODUTOS
             ON PRODUCAO_ORDEM_COR.PRODUTO = PRODUTOS.PRODUTO 
		 LEFT JOIN PRODUTOS_PRECOS PROD_PRECO_VENDA
			 ON PRODUCAO_ORDEM_COR.PRODUTO = PROD_PRECO_VENDA.PRODUTO AND PROD_PRECO_VENDA.CODIGO_TAB_PRECO = 'VO'
		LEFT JOIN PRODUCAO_PROG_PROD
			 ON PRODUCAO_ORDEM_COR.PRODUTO = PRODUCAO_PROG_PROD.PRODUTO AND PRODUCAO_ORDEM_COR.COR_PRODUTO = PRODUCAO_PROG_PROD.COR_PRODUTO AND PRODUCAO_ORDEM.PROGRAMACAO = PRODUCAO_PROG_PROD.PROGRAMACAO


        WHERE   PRODUCAO_ORDEM.PROGRAMACAO IN    (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                      WHERE ((PROPRIEDADE = '00060' 
                                             AND VALOR_PROPRIEDADE = 'VERAO 18') OR PROGRAMACAO LIKE '%V18%' OR PROGRAMACAO LIKE '%VER18%') 
                                             AND PROGRAMACAO IN (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                                                 WHERE PROPRIEDADE = '00038' 
                                                                 AND VALOR_PROPRIEDADE IN ('VAREJO','REPIQUE VAREJO'))) 
                 AND PRODUTOS.REDE_LOJAS = 1 
                 AND PRODUCAO_ORDEM.TIPO_PROCESSO = 0 
				 AND PRODUCAO_ORDEM_COR.QTDE_O <> 0

         GROUP BY PRODUCAO_ORDEM_COR.PRODUTO,
				  PRODUCAO_ORDEM_COR.COR_PRODUTO, 
				  PRODUCAO_PROG_PROD.ENTREGA_INICIAL,
				  PROD_PRECO_VENDA.PRECO1,
				  PRODUCAO_ORDEM.TIPO_ORDEM_PRODUCAO

union
     
         SELECT  CONCAT(RTRIM(LTRIM(COMPRAS_PRODUTO.PRODUTO)),RTRIM(LTRIM(COMPRAS_PRODUTO.COR_PRODUTO))) as REF,
         
				 COMPRAS_PRODUTO.ENTREGA as DATA,
				 SUM(COMPRAS_PRODUTO.QTDE_ORIGINAL) - ISNULL(SUM(COMPRAS_PROD_CANCELADA.QTDE_CANCELADA), 0) as QTDE,
				 PROD_PRECO_VENDA.PRECO1 AS PRECO_UNIT,
				 'VER18'  as COLECAO,
				 COMPRAS.TIPO_COMPRA as TIPO_PROGRAMACAO,
				 'COMPRA' as TIPO_TABELA
				 

         FROM COMPRAS_PRODUTO

         JOIN COMPRAS
             ON COMPRAS_PRODUTO.PEDIDO = COMPRAS.PEDIDO

         LEFT JOIN PRODUTOS
             ON COMPRAS_PRODUTO.PRODUTO = PRODUTOS.PRODUTO

         FULL JOIN COMPRAS_PROD_CANCELADA 
             ON COMPRAS_PRODUTO.PRODUTO = COMPRAS_PROD_CANCELADA.PRODUTO AND COMPRAS_PRODUTO.COR_PRODUTO = COMPRAS_PROD_CANCELADA.COR_PRODUTO AND COMPRAS_PRODUTO.PEDIDO = COMPRAS_PROD_CANCELADA.PEDIDO

		LEFT JOIN PRODUTOS_PRECOS PROD_PRECO_VENDA
			 ON COMPRAS_PRODUTO.PRODUTO = PROD_PRECO_VENDA.PRODUTO AND PROD_PRECO_VENDA.CODIGO_TAB_PRECO = 'VO'

         WHERE   COMPRAS.PROGRAMACAO IN    (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                      WHERE ((PROPRIEDADE = '00060' 
                                             AND VALOR_PROPRIEDADE = 'VERAO 18') OR PROGRAMACAO LIKE '%V18%' OR PROGRAMACAO LIKE '%VER18%') 
                                             AND PROGRAMACAO IN (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                                                 WHERE PROPRIEDADE = '00038' 
                                                                 AND VALOR_PROPRIEDADE IN ('VAREJO','REPIQUE VAREJO'))) 
                 AND PRODUTOS.REDE_LOJAS = 1 
				 AND COMPRAS_PRODUTO.QTDE_ORIGINAL <> 0
				 AND COMPRAS_PRODUTO.QTDE_CANCELADA <> COMPRAS_PRODUTO.QTDE_ORIGINAL

         GROUP BY COMPRAS_PRODUTO.PRODUTO,
				  COMPRAS_PRODUTO.COR_PRODUTO,
				  COMPRAS_PRODUTO.ENTREGA,
				  PROD_PRECO_VENDA.PRECO1,
				  COMPRAS.TIPO_COMPRA


		 

-------------------------------------------------------------------------------------------------------------------
UNION
---------------------------------------------VERAO 17--------------------------------------------------------------

SELECT  CONCAT(RTRIM(LTRIM(PRODUCAO_ORDEM_COR.PRODUTO)),RTRIM(LTRIM(PRODUCAO_ORDEM_COR.COR_PRODUTO))) as REF,

		PRODUCAO_PROG_PROD.ENTREGA_INICIAL as DATA,
		SUM(PRODUCAO_ORDEM_COR.QTDE_O) as QTDE,
		PROD_PRECO_VENDA.PRECO1 AS PRECO_UNIT,
		'VER17' as COLECAO,
		PRODUCAO_ORDEM.TIPO_ORDEM_PRODUCAO as TIPO_PROGRAMACAO,
		'COMPRA' as TIPO_TABELA
		
		
         FROM PRODUCAO_ORDEM_COR
         JOIN PRODUCAO_ORDEM
             ON PRODUCAO_ORDEM_COR.ORDEM_PRODUCAO = PRODUCAO_ORDEM.ORDEM_PRODUCAO AND PRODUCAO_ORDEM_COR.PRODUTO = PRODUCAO_ORDEM.PRODUTO 
         LEFT JOIN PRODUTOS
             ON PRODUCAO_ORDEM_COR.PRODUTO = PRODUTOS.PRODUTO 
		 LEFT JOIN PRODUTOS_PRECOS PROD_PRECO_VENDA
			 ON PRODUCAO_ORDEM_COR.PRODUTO = PROD_PRECO_VENDA.PRODUTO AND PROD_PRECO_VENDA.CODIGO_TAB_PRECO = 'VO'
		LEFT JOIN PRODUCAO_PROG_PROD
			 ON PRODUCAO_ORDEM_COR.PRODUTO = PRODUCAO_PROG_PROD.PRODUTO AND PRODUCAO_ORDEM_COR.COR_PRODUTO = PRODUCAO_PROG_PROD.COR_PRODUTO AND PRODUCAO_ORDEM.PROGRAMACAO = PRODUCAO_PROG_PROD.PROGRAMACAO


        WHERE   PRODUCAO_ORDEM.PROGRAMACAO IN    (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                      WHERE ((PROPRIEDADE = '00060' 
                                             AND VALOR_PROPRIEDADE = 'VERAO 17') OR PROGRAMACAO LIKE '%V17%' OR PROGRAMACAO LIKE '%VER17%') 
                                             AND PROGRAMACAO IN (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                                                 WHERE PROPRIEDADE = '00038' 
                                                                 AND VALOR_PROPRIEDADE IN ('VAREJO','REPIQUE VAREJO'))) 
                 AND PRODUTOS.REDE_LOJAS = 1 
                 AND PRODUCAO_ORDEM.TIPO_PROCESSO = 0 
				 AND PRODUCAO_ORDEM_COR.QTDE_O <> 0

         GROUP BY PRODUCAO_ORDEM_COR.PRODUTO,
				  PRODUCAO_ORDEM_COR.COR_PRODUTO, 
				  PRODUCAO_PROG_PROD.ENTREGA_INICIAL,
				  PROD_PRECO_VENDA.PRECO1,
				  PRODUCAO_ORDEM.TIPO_ORDEM_PRODUCAO

union
     
         SELECT  CONCAT(RTRIM(LTRIM(COMPRAS_PRODUTO.PRODUTO)),RTRIM(LTRIM(COMPRAS_PRODUTO.COR_PRODUTO))) as REF,
         
				 COMPRAS_PRODUTO.ENTREGA as DATA,
				 SUM(COMPRAS_PRODUTO.QTDE_ORIGINAL) - ISNULL(SUM(COMPRAS_PROD_CANCELADA.QTDE_CANCELADA), 0) as QTDE,
				 PROD_PRECO_VENDA.PRECO1 AS PRECO_UNIT,
				 'VER17'  as COLECAO,
				 COMPRAS.TIPO_COMPRA as TIPO_PROGRAMACAO,
				 'COMPRA' as TIPO_TABELA
				 

         FROM COMPRAS_PRODUTO

         JOIN COMPRAS
             ON COMPRAS_PRODUTO.PEDIDO = COMPRAS.PEDIDO

         LEFT JOIN PRODUTOS
             ON COMPRAS_PRODUTO.PRODUTO = PRODUTOS.PRODUTO

         FULL JOIN COMPRAS_PROD_CANCELADA 
             ON COMPRAS_PRODUTO.PRODUTO = COMPRAS_PROD_CANCELADA.PRODUTO AND COMPRAS_PRODUTO.COR_PRODUTO = COMPRAS_PROD_CANCELADA.COR_PRODUTO AND COMPRAS_PRODUTO.PEDIDO = COMPRAS_PROD_CANCELADA.PEDIDO

		LEFT JOIN PRODUTOS_PRECOS PROD_PRECO_VENDA
			 ON COMPRAS_PRODUTO.PRODUTO = PROD_PRECO_VENDA.PRODUTO AND PROD_PRECO_VENDA.CODIGO_TAB_PRECO = 'VO'

         WHERE   COMPRAS.PROGRAMACAO IN    (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                      WHERE ((PROPRIEDADE = '00060' 
                                             AND VALOR_PROPRIEDADE = 'VERAO 17') OR PROGRAMACAO LIKE '%V17%' OR PROGRAMACAO LIKE '%VER17%') 
                                             AND PROGRAMACAO IN (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                                                 WHERE PROPRIEDADE = '00038' 
                                                                 AND VALOR_PROPRIEDADE IN ('VAREJO','REPIQUE VAREJO'))) 
                 AND PRODUTOS.REDE_LOJAS = 1 
				 AND COMPRAS_PRODUTO.QTDE_ORIGINAL <> 0
				 AND COMPRAS_PRODUTO.QTDE_CANCELADA <> COMPRAS_PRODUTO.QTDE_ORIGINAL

         GROUP BY COMPRAS_PRODUTO.PRODUTO,
				  COMPRAS_PRODUTO.COR_PRODUTO,
				  COMPRAS_PRODUTO.ENTREGA,
				  PROD_PRECO_VENDA.PRECO1,
				  COMPRAS.TIPO_COMPRA


		 
-------------------------------------------------------------------------------------------------------------------
UNION
---------------------------------------------INVERNO 20------------------------------------------------------------

SELECT  CONCAT(RTRIM(LTRIM(PRODUCAO_ORDEM_COR.PRODUTO)),RTRIM(LTRIM(PRODUCAO_ORDEM_COR.COR_PRODUTO))) as REF,

		PRODUCAO_PROG_PROD.ENTREGA_INICIAL as DATA,
		SUM(PRODUCAO_ORDEM_COR.QTDE_O) as QTDE,
		PROD_PRECO_VENDA.PRECO1 AS PRECO_UNIT,
		'INV20' as COLECAO,
		PRODUCAO_ORDEM.TIPO_ORDEM_PRODUCAO as TIPO_PROGRAMACAO,
		'COMPRA' as TIPO_TABELA
		
		
         FROM PRODUCAO_ORDEM_COR
         JOIN PRODUCAO_ORDEM
             ON PRODUCAO_ORDEM_COR.ORDEM_PRODUCAO = PRODUCAO_ORDEM.ORDEM_PRODUCAO AND PRODUCAO_ORDEM_COR.PRODUTO = PRODUCAO_ORDEM.PRODUTO 
         LEFT JOIN PRODUTOS
             ON PRODUCAO_ORDEM_COR.PRODUTO = PRODUTOS.PRODUTO 
		 LEFT JOIN PRODUTOS_PRECOS PROD_PRECO_VENDA
			 ON PRODUCAO_ORDEM_COR.PRODUTO = PROD_PRECO_VENDA.PRODUTO AND PROD_PRECO_VENDA.CODIGO_TAB_PRECO = 'VO'
		LEFT JOIN PRODUCAO_PROG_PROD
			 ON PRODUCAO_ORDEM_COR.PRODUTO = PRODUCAO_PROG_PROD.PRODUTO AND PRODUCAO_ORDEM_COR.COR_PRODUTO = PRODUCAO_PROG_PROD.COR_PRODUTO AND PRODUCAO_ORDEM.PROGRAMACAO = PRODUCAO_PROG_PROD.PROGRAMACAO


        WHERE   PRODUCAO_ORDEM.PROGRAMACAO IN    (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                      WHERE ((PROPRIEDADE = '00060' 
                                             AND VALOR_PROPRIEDADE = 'INVERNO 20') OR PROGRAMACAO LIKE '%I20%' OR PROGRAMACAO LIKE '%INV20%') 
                                             AND PROGRAMACAO IN (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                                                 WHERE PROPRIEDADE = '00038' 
                                                                 AND VALOR_PROPRIEDADE IN ('VAREJO','REPIQUE VAREJO'))) 
                 AND PRODUTOS.REDE_LOJAS = 1 
                 AND PRODUCAO_ORDEM.TIPO_PROCESSO = 0 
				 AND PRODUCAO_ORDEM_COR.QTDE_O <> 0

         GROUP BY PRODUCAO_ORDEM_COR.PRODUTO,
				  PRODUCAO_ORDEM_COR.COR_PRODUTO, 
				  PRODUCAO_PROG_PROD.ENTREGA_INICIAL,
				  PROD_PRECO_VENDA.PRECO1,
				  PRODUCAO_ORDEM.TIPO_ORDEM_PRODUCAO

union
     
         SELECT  CONCAT(RTRIM(LTRIM(COMPRAS_PRODUTO.PRODUTO)),RTRIM(LTRIM(COMPRAS_PRODUTO.COR_PRODUTO))) as REF,
         
				 COMPRAS_PRODUTO.ENTREGA as DATA,
				 SUM(COMPRAS_PRODUTO.QTDE_ORIGINAL) - ISNULL(SUM(COMPRAS_PROD_CANCELADA.QTDE_CANCELADA), 0) as QTDE,
				 PROD_PRECO_VENDA.PRECO1 AS PRECO_UNIT,
				 'INV20'  as COLECAO,
				 COMPRAS.TIPO_COMPRA as TIPO_PROGRAMACAO,
				 'COMPRA' as TIPO_TABELA
				 

         FROM COMPRAS_PRODUTO

         JOIN COMPRAS
             ON COMPRAS_PRODUTO.PEDIDO = COMPRAS.PEDIDO

         LEFT JOIN PRODUTOS
             ON COMPRAS_PRODUTO.PRODUTO = PRODUTOS.PRODUTO

         FULL JOIN COMPRAS_PROD_CANCELADA 
             ON COMPRAS_PRODUTO.PRODUTO = COMPRAS_PROD_CANCELADA.PRODUTO AND COMPRAS_PRODUTO.COR_PRODUTO = COMPRAS_PROD_CANCELADA.COR_PRODUTO AND COMPRAS_PRODUTO.PEDIDO = COMPRAS_PROD_CANCELADA.PEDIDO

		LEFT JOIN PRODUTOS_PRECOS PROD_PRECO_VENDA
			 ON COMPRAS_PRODUTO.PRODUTO = PROD_PRECO_VENDA.PRODUTO AND PROD_PRECO_VENDA.CODIGO_TAB_PRECO = 'VO'

         WHERE   COMPRAS.PROGRAMACAO IN    (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                      WHERE ((PROPRIEDADE = '00060' 
                                             AND VALOR_PROPRIEDADE = 'INVERNO 20') OR PROGRAMACAO LIKE '%I20%' OR PROGRAMACAO LIKE '%INV20%') 
                                             AND PROGRAMACAO IN (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                                                 WHERE PROPRIEDADE = '00038' 
                                                                 AND VALOR_PROPRIEDADE IN ('VAREJO','REPIQUE VAREJO'))) 
                 AND PRODUTOS.REDE_LOJAS = 1 
				 AND COMPRAS_PRODUTO.QTDE_ORIGINAL <> 0
				 AND COMPRAS_PRODUTO.QTDE_CANCELADA <> COMPRAS_PRODUTO.QTDE_ORIGINAL

         GROUP BY COMPRAS_PRODUTO.PRODUTO,
				  COMPRAS_PRODUTO.COR_PRODUTO,
				  COMPRAS_PRODUTO.ENTREGA,
				  PROD_PRECO_VENDA.PRECO1,
				  COMPRAS.TIPO_COMPRA
-------------------------------------------------------------------------------------------------------------------
UNION
---------------------------------------------INVERNO 19------------------------------------------------------------

SELECT  CONCAT(RTRIM(LTRIM(PRODUCAO_ORDEM_COR.PRODUTO)),RTRIM(LTRIM(PRODUCAO_ORDEM_COR.COR_PRODUTO))) as REF,

		PRODUCAO_PROG_PROD.ENTREGA_INICIAL as DATA,
		SUM(PRODUCAO_ORDEM_COR.QTDE_O) as QTDE,
		PROD_PRECO_VENDA.PRECO1 AS PRECO_UNIT,
		'INV19' as COLECAO,
		PRODUCAO_ORDEM.TIPO_ORDEM_PRODUCAO as TIPO_PROGRAMACAO,
		'COMPRA' as TIPO_TABELA
		
		
         FROM PRODUCAO_ORDEM_COR
         JOIN PRODUCAO_ORDEM
             ON PRODUCAO_ORDEM_COR.ORDEM_PRODUCAO = PRODUCAO_ORDEM.ORDEM_PRODUCAO AND PRODUCAO_ORDEM_COR.PRODUTO = PRODUCAO_ORDEM.PRODUTO 
         LEFT JOIN PRODUTOS
             ON PRODUCAO_ORDEM_COR.PRODUTO = PRODUTOS.PRODUTO 
		 LEFT JOIN PRODUTOS_PRECOS PROD_PRECO_VENDA
			 ON PRODUCAO_ORDEM_COR.PRODUTO = PROD_PRECO_VENDA.PRODUTO AND PROD_PRECO_VENDA.CODIGO_TAB_PRECO = 'VO'
		LEFT JOIN PRODUCAO_PROG_PROD
			 ON PRODUCAO_ORDEM_COR.PRODUTO = PRODUCAO_PROG_PROD.PRODUTO AND PRODUCAO_ORDEM_COR.COR_PRODUTO = PRODUCAO_PROG_PROD.COR_PRODUTO AND PRODUCAO_ORDEM.PROGRAMACAO = PRODUCAO_PROG_PROD.PROGRAMACAO


        WHERE   PRODUCAO_ORDEM.PROGRAMACAO IN    (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                      WHERE ((PROPRIEDADE = '00060' 
                                             AND VALOR_PROPRIEDADE = 'INVERNO 19') OR PROGRAMACAO LIKE '%I19%' OR PROGRAMACAO LIKE '%INV19%') 
                                             AND PROGRAMACAO IN (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                                                 WHERE PROPRIEDADE = '00038' 
                                                                 AND VALOR_PROPRIEDADE IN ('VAREJO','REPIQUE VAREJO'))) 
                 AND PRODUTOS.REDE_LOJAS = 1 
                 AND PRODUCAO_ORDEM.TIPO_PROCESSO = 0 
				 AND PRODUCAO_ORDEM_COR.QTDE_O <> 0

         GROUP BY PRODUCAO_ORDEM_COR.PRODUTO,
				  PRODUCAO_ORDEM_COR.COR_PRODUTO, 
				  PRODUCAO_PROG_PROD.ENTREGA_INICIAL,
				  PROD_PRECO_VENDA.PRECO1,
				  PRODUCAO_ORDEM.TIPO_ORDEM_PRODUCAO

union
     
         SELECT  CONCAT(RTRIM(LTRIM(COMPRAS_PRODUTO.PRODUTO)),RTRIM(LTRIM(COMPRAS_PRODUTO.COR_PRODUTO))) as REF,
         
				 COMPRAS_PRODUTO.ENTREGA as DATA,
				 SUM(COMPRAS_PRODUTO.QTDE_ORIGINAL) - ISNULL(SUM(COMPRAS_PROD_CANCELADA.QTDE_CANCELADA), 0) as QTDE,
				 PROD_PRECO_VENDA.PRECO1 AS PRECO_UNIT,
				 'INV19'  as COLECAO,
				 COMPRAS.TIPO_COMPRA as TIPO_PROGRAMACAO,
				 'COMPRA' as TIPO_TABELA
				 

         FROM COMPRAS_PRODUTO

         JOIN COMPRAS
             ON COMPRAS_PRODUTO.PEDIDO = COMPRAS.PEDIDO

         LEFT JOIN PRODUTOS
             ON COMPRAS_PRODUTO.PRODUTO = PRODUTOS.PRODUTO

         FULL JOIN COMPRAS_PROD_CANCELADA 
             ON COMPRAS_PRODUTO.PRODUTO = COMPRAS_PROD_CANCELADA.PRODUTO AND COMPRAS_PRODUTO.COR_PRODUTO = COMPRAS_PROD_CANCELADA.COR_PRODUTO AND COMPRAS_PRODUTO.PEDIDO = COMPRAS_PROD_CANCELADA.PEDIDO

		LEFT JOIN PRODUTOS_PRECOS PROD_PRECO_VENDA
			 ON COMPRAS_PRODUTO.PRODUTO = PROD_PRECO_VENDA.PRODUTO AND PROD_PRECO_VENDA.CODIGO_TAB_PRECO = 'VO'

         WHERE   COMPRAS.PROGRAMACAO IN    (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                      WHERE ((PROPRIEDADE = '00060' 
                                             AND VALOR_PROPRIEDADE = 'INVERNO 19') OR PROGRAMACAO LIKE '%I19%' OR PROGRAMACAO LIKE '%INV19%') 
                                             AND PROGRAMACAO IN (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                                                 WHERE PROPRIEDADE = '00038' 
                                                                 AND VALOR_PROPRIEDADE IN ('VAREJO','REPIQUE VAREJO'))) 
                 AND PRODUTOS.REDE_LOJAS = 1 
				 AND COMPRAS_PRODUTO.QTDE_ORIGINAL <> 0
				 AND COMPRAS_PRODUTO.QTDE_CANCELADA <> COMPRAS_PRODUTO.QTDE_ORIGINAL

         GROUP BY COMPRAS_PRODUTO.PRODUTO,
				  COMPRAS_PRODUTO.COR_PRODUTO,
				  COMPRAS_PRODUTO.ENTREGA,
				  PROD_PRECO_VENDA.PRECO1,
				  COMPRAS.TIPO_COMPRA
-------------------------------------------------------------------------------------------------------------------
UNION
---------------------------------------------INVERNO 18------------------------------------------------------------

SELECT  CONCAT(RTRIM(LTRIM(PRODUCAO_ORDEM_COR.PRODUTO)),RTRIM(LTRIM(PRODUCAO_ORDEM_COR.COR_PRODUTO))) as REF,

		PRODUCAO_PROG_PROD.ENTREGA_INICIAL as DATA,
		SUM(PRODUCAO_ORDEM_COR.QTDE_O) as QTDE,
		PROD_PRECO_VENDA.PRECO1 AS PRECO_UNIT,
		'INV18' as COLEÇÃO,
		PRODUCAO_ORDEM.TIPO_ORDEM_PRODUCAO as TIPO_PROGRAMACAO,
		'COMPRA' as TIPO_TABELA
		
		
         FROM PRODUCAO_ORDEM_COR
         JOIN PRODUCAO_ORDEM
             ON PRODUCAO_ORDEM_COR.ORDEM_PRODUCAO = PRODUCAO_ORDEM.ORDEM_PRODUCAO AND PRODUCAO_ORDEM_COR.PRODUTO = PRODUCAO_ORDEM.PRODUTO 
         LEFT JOIN PRODUTOS
             ON PRODUCAO_ORDEM_COR.PRODUTO = PRODUTOS.PRODUTO 
		 LEFT JOIN PRODUTOS_PRECOS PROD_PRECO_VENDA
			 ON PRODUCAO_ORDEM_COR.PRODUTO = PROD_PRECO_VENDA.PRODUTO AND PROD_PRECO_VENDA.CODIGO_TAB_PRECO = 'VO'
		LEFT JOIN PRODUCAO_PROG_PROD
			 ON PRODUCAO_ORDEM_COR.PRODUTO = PRODUCAO_PROG_PROD.PRODUTO AND PRODUCAO_ORDEM_COR.COR_PRODUTO = PRODUCAO_PROG_PROD.COR_PRODUTO AND PRODUCAO_ORDEM.PROGRAMACAO = PRODUCAO_PROG_PROD.PROGRAMACAO


        WHERE   PRODUCAO_ORDEM.PROGRAMACAO IN    (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                      WHERE ((PROPRIEDADE = '00060' 
                                             AND VALOR_PROPRIEDADE = 'INVERNO 18') OR PROGRAMACAO LIKE '%I18%' OR PROGRAMACAO LIKE '%INV18%') 
                                             AND PROGRAMACAO IN (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                                                 WHERE PROPRIEDADE = '00038' 
                                                                 AND VALOR_PROPRIEDADE IN ('VAREJO','REPIQUE VAREJO'))) 
                 AND PRODUTOS.REDE_LOJAS = 1 
                 AND PRODUCAO_ORDEM.TIPO_PROCESSO = 0 
				 AND PRODUCAO_ORDEM_COR.QTDE_O <> 0

         GROUP BY PRODUCAO_ORDEM_COR.PRODUTO,
				  PRODUCAO_ORDEM_COR.COR_PRODUTO, 
				  PRODUCAO_PROG_PROD.ENTREGA_INICIAL,
				  PROD_PRECO_VENDA.PRECO1,
				  PRODUCAO_ORDEM.TIPO_ORDEM_PRODUCAO

union
     
         SELECT  CONCAT(RTRIM(LTRIM(COMPRAS_PRODUTO.PRODUTO)),RTRIM(LTRIM(COMPRAS_PRODUTO.COR_PRODUTO))) as REF,
         
				 COMPRAS_PRODUTO.ENTREGA as DATA,
				 SUM(COMPRAS_PRODUTO.QTDE_ORIGINAL) - ISNULL(SUM(COMPRAS_PROD_CANCELADA.QTDE_CANCELADA), 0) as QTDE,
				 PROD_PRECO_VENDA.PRECO1 AS PRECO_UNIT,
				 'INV18'  as COLEÇÃO,
				 COMPRAS.TIPO_COMPRA as TIPO_PROGRAMACAO,
				 'COMPRA' as TIPO_TABELA
				 

         FROM COMPRAS_PRODUTO

         JOIN COMPRAS
             ON COMPRAS_PRODUTO.PEDIDO = COMPRAS.PEDIDO

         LEFT JOIN PRODUTOS
             ON COMPRAS_PRODUTO.PRODUTO = PRODUTOS.PRODUTO

         FULL JOIN COMPRAS_PROD_CANCELADA 
             ON COMPRAS_PRODUTO.PRODUTO = COMPRAS_PROD_CANCELADA.PRODUTO AND COMPRAS_PRODUTO.COR_PRODUTO = COMPRAS_PROD_CANCELADA.COR_PRODUTO AND COMPRAS_PRODUTO.PEDIDO = COMPRAS_PROD_CANCELADA.PEDIDO

		LEFT JOIN PRODUTOS_PRECOS PROD_PRECO_VENDA
			 ON COMPRAS_PRODUTO.PRODUTO = PROD_PRECO_VENDA.PRODUTO AND PROD_PRECO_VENDA.CODIGO_TAB_PRECO = 'VO'

         WHERE   COMPRAS.PROGRAMACAO IN    (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                      WHERE ((PROPRIEDADE = '00060' 
                                             AND VALOR_PROPRIEDADE = 'INVERNO 18') OR PROGRAMACAO LIKE '%I18%' OR PROGRAMACAO LIKE '%INV18%') 
                                             AND PROGRAMACAO IN (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                                                 WHERE PROPRIEDADE = '00038' 
                                                                 AND VALOR_PROPRIEDADE IN ('VAREJO','REPIQUE VAREJO'))) 
                 AND PRODUTOS.REDE_LOJAS = 1 
				 AND COMPRAS_PRODUTO.QTDE_ORIGINAL <> 0
				 AND COMPRAS_PRODUTO.QTDE_CANCELADA <> COMPRAS_PRODUTO.QTDE_ORIGINAL

         GROUP BY COMPRAS_PRODUTO.PRODUTO,
				  COMPRAS_PRODUTO.COR_PRODUTO,
				  COMPRAS_PRODUTO.ENTREGA,
				  PROD_PRECO_VENDA.PRECO1,
				  COMPRAS.TIPO_COMPRA

-------------------------------------------------------------------------------------------------------------------
UNION
---------------------------------------------INVERNO 17------------------------------------------------------------

SELECT  CONCAT(RTRIM(LTRIM(PRODUCAO_ORDEM_COR.PRODUTO)),RTRIM(LTRIM(PRODUCAO_ORDEM_COR.COR_PRODUTO))) as REF,

		PRODUCAO_PROG_PROD.ENTREGA_INICIAL as DATA,
		SUM(PRODUCAO_ORDEM_COR.QTDE_O) as QTDE,
		PROD_PRECO_VENDA.PRECO1 AS PRECO_UNIT,
		'INV 17' as COLEÇÃO,
		PRODUCAO_ORDEM.TIPO_ORDEM_PRODUCAO as TIPO_PROGRAMACAO,
		'COMPRA' as TIPO_TABELA
		
		
         FROM PRODUCAO_ORDEM_COR
         JOIN PRODUCAO_ORDEM
             ON PRODUCAO_ORDEM_COR.ORDEM_PRODUCAO = PRODUCAO_ORDEM.ORDEM_PRODUCAO AND PRODUCAO_ORDEM_COR.PRODUTO = PRODUCAO_ORDEM.PRODUTO 
         LEFT JOIN PRODUTOS
             ON PRODUCAO_ORDEM_COR.PRODUTO = PRODUTOS.PRODUTO 
		 LEFT JOIN PRODUTOS_PRECOS PROD_PRECO_VENDA
			 ON PRODUCAO_ORDEM_COR.PRODUTO = PROD_PRECO_VENDA.PRODUTO AND PROD_PRECO_VENDA.CODIGO_TAB_PRECO = 'VO'
		LEFT JOIN PRODUCAO_PROG_PROD
			 ON PRODUCAO_ORDEM_COR.PRODUTO = PRODUCAO_PROG_PROD.PRODUTO AND PRODUCAO_ORDEM_COR.COR_PRODUTO = PRODUCAO_PROG_PROD.COR_PRODUTO AND PRODUCAO_ORDEM.PROGRAMACAO = PRODUCAO_PROG_PROD.PROGRAMACAO


        WHERE   PRODUCAO_ORDEM.PROGRAMACAO IN    (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                      WHERE ((PROPRIEDADE = '00060' 
                                             AND VALOR_PROPRIEDADE = 'INVERNO 17') OR PROGRAMACAO LIKE '%I17%' OR PROGRAMACAO LIKE '%INV17%') 
                                             AND PROGRAMACAO IN (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                                                 WHERE PROPRIEDADE = '00038' 
                                                                 AND VALOR_PROPRIEDADE IN ('VAREJO','REPIQUE VAREJO'))) 
                 AND PRODUTOS.REDE_LOJAS = 1 
                 AND PRODUCAO_ORDEM.TIPO_PROCESSO = 0 
				 AND PRODUCAO_ORDEM_COR.QTDE_O <> 0

         GROUP BY PRODUCAO_ORDEM_COR.PRODUTO,
				  PRODUCAO_ORDEM_COR.COR_PRODUTO, 
				  PRODUCAO_PROG_PROD.ENTREGA_INICIAL,
				  PROD_PRECO_VENDA.PRECO1,
				  PRODUCAO_ORDEM.TIPO_ORDEM_PRODUCAO

union
     
         SELECT  CONCAT(RTRIM(LTRIM(COMPRAS_PRODUTO.PRODUTO)),RTRIM(LTRIM(COMPRAS_PRODUTO.COR_PRODUTO))) as REF,
				 COMPRAS_PRODUTO.ENTREGA as DATA,
				 SUM(COMPRAS_PRODUTO.QTDE_ORIGINAL) - ISNULL(SUM(COMPRAS_PROD_CANCELADA.QTDE_CANCELADA), 0) as QTDE,
				 PROD_PRECO_VENDA.PRECO1 AS PRECO_UNIT,
				 'INV 17'  as COLEÇÃO,
				 COMPRAS.TIPO_COMPRA as TIPO_PROGRAMACAO,
				 'COMPRA' as TIPO_TABELA
				 

         FROM COMPRAS_PRODUTO

         JOIN COMPRAS
             ON COMPRAS_PRODUTO.PEDIDO = COMPRAS.PEDIDO

         LEFT JOIN PRODUTOS
             ON COMPRAS_PRODUTO.PRODUTO = PRODUTOS.PRODUTO

         FULL JOIN COMPRAS_PROD_CANCELADA 
             ON COMPRAS_PRODUTO.PRODUTO = COMPRAS_PROD_CANCELADA.PRODUTO AND COMPRAS_PRODUTO.COR_PRODUTO = COMPRAS_PROD_CANCELADA.COR_PRODUTO AND COMPRAS_PRODUTO.PEDIDO = COMPRAS_PROD_CANCELADA.PEDIDO

		LEFT JOIN PRODUTOS_PRECOS PROD_PRECO_VENDA
			 ON COMPRAS_PRODUTO.PRODUTO = PROD_PRECO_VENDA.PRODUTO AND PROD_PRECO_VENDA.CODIGO_TAB_PRECO = 'VO'

         WHERE   COMPRAS.PROGRAMACAO IN    (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                      WHERE ((PROPRIEDADE = '00060' 
                                             AND VALOR_PROPRIEDADE = 'INVERNO 17') OR PROGRAMACAO LIKE '%I17%' OR PROGRAMACAO LIKE '%INV17%') 
                                             AND PROGRAMACAO IN (SELECT DISTINCT PROGRAMACAO FROM PROP_PRODUCAO_PROGRAMA 
                                                                 WHERE PROPRIEDADE = '00038' 
                                                                 AND VALOR_PROPRIEDADE IN ('VAREJO','REPIQUE VAREJO'))) 
                 AND PRODUTOS.REDE_LOJAS = 1 
				 AND COMPRAS_PRODUTO.QTDE_ORIGINAL <> 0
				 AND COMPRAS_PRODUTO.QTDE_CANCELADA <> COMPRAS_PRODUTO.QTDE_ORIGINAL

         GROUP BY COMPRAS_PRODUTO.PRODUTO,
				  COMPRAS_PRODUTO.COR_PRODUTO,
				  COMPRAS_PRODUTO.ENTREGA,
				  PROD_PRECO_VENDA.PRECO1,
				  COMPRAS.TIPO_COMPRA